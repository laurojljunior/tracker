#ifndef SERIAL_H_
#define SERIAL_H_

#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>
#include <wiringSerial.h>

int open_serial(const char* device, int baud);
int write_serial(int fd, char* stream);
int read_serial(int fd);
int close_serial(int fd);



#endif /* SERIAL_H_ */
