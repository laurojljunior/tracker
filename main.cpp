#include <opencv2/opencv.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <thread>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include "serial.h"
#include "socket/ServerSocket.hpp"

#include <opentracker/eco/eco.hpp>
#include <opentracker/eco/parameters.hpp>
#include <opentracker/eco/metrics.hpp>
#include <opentracker/eco/debug.hpp>

#include <raspicam/raspicam.h>

using namespace std;
using namespace cv;
using namespace eco;

char *CAPTURE_TYPE = (char *)"rpi";
int IMAGE_WIDTH = 360;
int IMAGE_HEIGHT = 240;
int TRACKER_SCALE = 3;
int TRACKER_TRAINING_GAP = 15;
char *VIDEO_FILE_NAME = NULL;

// declares all required variables
Rect2f roi;
Point2i point;
int drag = 0;
int web_live_view_is_updated;
Mat web_live_view_img;

cv::VideoCapture* cap;
Mat frame, r_frame;

unsigned char* rpi_cam_data = NULL;
raspicam::RaspiCam RpiCamera;

const int RPI_CAMERA_WIDTH_RESOLUTION = 1024;
const int RPI_CAMERA_HEIGHT_RESOLUTION = 768;

double
get_time(void)
{
	struct timeval tv;
	double t;

	if (gettimeofday(&tv, NULL) < 0)
		printf("as1_get_time encountered error in gettimeofday : %s\n",
				strerror(errno));
	t = tv.tv_sec + tv.tv_usec/1000000.0;
	return t;
}

char** str_split(char* a_str, const char a_delim)
{
	char** result    = 0;
	size_t count     = 0;
	char* tmp        = a_str;
	char* last_comma = 0;
	char delim[2];
	delim[0] = a_delim;
	delim[1] = 0;

	/* Count how many elements will be extracted. */
	while (*tmp)
	{
		if (a_delim == *tmp)
		{
			count++;
			last_comma = tmp;
		}
		tmp++;
	}

	/* Add space for trailing token. */
	count += last_comma < (a_str + strlen(a_str) - 1);

	/* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
	count++;

	result = (char**) malloc(sizeof(char*) * count);

	if (result)
	{
		size_t idx  = 0;
		char* token = strtok(a_str, delim);

		while (token)
		{
			assert(idx < count);
			*(result + idx++) = strdup(token);
			token = strtok(0, delim);
		}
		assert(idx == count - 1);
		*(result + idx) = 0;
	}

	return result;
}

bool is_file_modified(char* database_file, time_t& oldMTime)
{
	struct stat file_stat;
	int err = stat(database_file, &file_stat);
	if (err != 0) {
		perror(" [file_is_modified] stat");
		exit(0);
	}

	bool result = file_stat.st_mtime > oldMTime;
	oldMTime = file_stat.st_mtime;

	return result;
}

void read_web_parameters(char* filename)
{
	cout << "Reading web parameters." << endl;

	char ** items;
	std::ifstream infile(filename);

	std::string line;
	while (std::getline(infile, line))
	{
		items = str_split((char*) line.c_str(), ',');
		int x1 = atoi(items[0]);
		int y1 = atoi(items[1]);
		int x2 = atoi(items[2]);
		int y2 = atoi(items[3]);

		roi = cv::Rect(x1, y1, x2 - x1, y2 - y1);

		cout << "web roi: " << roi << endl;
	}

	infile.close();
}


void mouseHandler(int event, int x, int y, int flags, void* param)
{
	/* user press left button */
	if (event == CV_EVENT_LBUTTONDOWN && !drag)
	{
		printf("left click\n");
		point = cvPoint(x, y);
		drag = 1;
	}
	/* user drag the mouse */
	if (event == CV_EVENT_MOUSEMOVE && drag)
	{
		printf("dragging...\n");
		roi = Rect(point.x,point.y,x-point.x,y-point.y);
	}

	/* user release left button */
	if (event == CV_EVENT_LBUTTONUP && drag)
	{
		roi = Rect(point.x,point.y,x-point.x,y-point.y);
		drag = 0;
	}

	/* user click right button: reset all */
	if (event == CV_EVENT_RBUTTONUP)
	{
		drag = 0;
	}
}

void web_live_view_handler(int* web_live_view_is_updated, cv::Mat* web_live_view_img)
{
	printf("Started Web Thread\n");
	signal(SIGPIPE, SIG_IGN);

	int ret = 0;

	// Create the socket
	ServerSocket server ( 8888 );

	vector<uchar> imgBuffer;
	vector<int> param;
	param.push_back(CV_IMWRITE_JPEG_QUALITY);
	param.push_back(50);

	char headerBuffer[512];

	while ( true )
	{
		printf("Waiting connection...\n");
		ServerSocket new_sock;
		server.accept ( new_sock );
		string data;
		new_sock >> data;

		if(data.size())
		{
			printf("Connection accepted.\n");

			//cout<<"Received Data:\n"<<data<<"\n"<<endl;
			sprintf(headerBuffer,"HTTP/1.1 200 OK\r\n"
			  "Content-Type: multipart/x-mixed-replace; boundary=boundarydonotcross\r\n");

			new_sock.write(headerBuffer,strlen(headerBuffer), &ret);

			double timestamp;
			struct timeval tv;

			while(true)
			{
                usleep(100);
				if(*web_live_view_is_updated)
				{
					if(!web_live_view_img->empty())
					{
						gettimeofday(&tv,NULL);
						timestamp = tv.tv_sec+tv.tv_usec/1000000.0;

						sprintf(headerBuffer,"%lf.jpg", timestamp);
						imencode(".jpg",*web_live_view_img,imgBuffer,param);

						//"X-Timestamp: %.06lf\r\n"
						sprintf(headerBuffer,"\r\n--boundarydonotcross\r\n"
							  "Content-Type: image/jpeg\r\n"
							  "Content-Length: %ld\r\n"
							  "\r\n",
							  imgBuffer.size());

						new_sock.write(headerBuffer,strlen(headerBuffer), &ret);
						new_sock.write((unsigned char *)imgBuffer.data(),imgBuffer.size(), &ret);

						sprintf(headerBuffer,"\r\n");
						new_sock.write(headerBuffer,strlen(headerBuffer), &ret);

						if(ret == 1)
							break;
					}

					*web_live_view_is_updated = 0;
				}
			}
		}
	}
}


void capture_configuration(char* type, char* file)
{
	if(!strcmp(type, "rpi"))
	{
		printf("RPI Camera selected.\n");

		RpiCamera.setBrightness(55);
		RpiCamera.setContrast(10);
		RpiCamera.setMetering(raspicam::RASPICAM_METERING_MATRIX);

		RpiCamera.setFormat(raspicam::RASPICAM_FORMAT_RGB);
		RpiCamera.setBitRate(12000000);
		RpiCamera.setWidth(RPI_CAMERA_WIDTH_RESOLUTION);
		RpiCamera.setHeight(RPI_CAMERA_HEIGHT_RESOLUTION);
		RpiCamera.setFrameRate(15);
		//RpiCamera.setHorizontalFlip(true);
		RpiCamera.setVerticalFlip(true);

		//Open camera
		if (!RpiCamera.open()) {cerr<<"Error opening the camera"<<endl; exit(0);}
		sleep(3);

		rpi_cam_data = (unsigned char* )calloc (RPI_CAMERA_WIDTH_RESOLUTION * RPI_CAMERA_HEIGHT_RESOLUTION * 3, sizeof(unsigned char));
	}
	else if(!strcmp(type, "file"))
	{
		printf("File stream selected.\n");

		if(file != NULL)
			cap = new cv::VideoCapture(file);
		else
		{
			printf("File path is NULL. Aborting...\n");
			exit(0);
		}
	}
	else if(!strcmp(type, "cam"))
	{
		printf("WebCam stream selected.\n");

		cap = new cv::VideoCapture(1);
	}
	else
	{
		printf("Not recognized capture type. Aborting...\n");
		exit(0);
	}
}

void read_parameters(int argc, char* argv[])
{
	if(argc < 6)
	{
		printf("Usage: ./tracker [rpi | file | cam] [width] [height] [scale] [traning gap] {file path}\n");
		exit(0);
	}
	else
	{
		CAPTURE_TYPE = strdup(argv[1]);
		IMAGE_WIDTH = atoi(argv[2]);
		IMAGE_HEIGHT = atoi(argv[3]);
		TRACKER_SCALE = atoi(argv[4]);
		TRACKER_TRAINING_GAP = atoi(argv[5]);

		if(!strcmp(CAPTURE_TYPE, "file") && argc == 7)
			VIDEO_FILE_NAME = strdup(argv[6]);
	}
}

int main( int argc, char** argv ){

	int serial_fd;
	read_parameters(argc, argv);
	capture_configuration(CAPTURE_TYPE, VIDEO_FILE_NAME);

	// create a tracker object
	ECO ecotracker;
	eco::EcoParameters parameters;

	parameters.useCnFeature = false;
	parameters.number_of_scales = TRACKER_SCALE;
	parameters.train_gap = TRACKER_TRAINING_GAP;
	parameters.cn_features.fparams.tablename = "/usr/local/include/opentracker/eco/look_tables/CNnorm.txt";

#ifdef ENABLE_GUI
	namedWindow("view", CV_WINDOW_AUTOSIZE);
	setMouseCallback("view", mouseHandler, NULL);
#endif

	serial_fd = open_serial("/dev/serial0", 9600);

    std::thread web_live_view_thread (web_live_view_handler, &web_live_view_is_updated, &web_live_view_img);

#ifdef ENABLE_GUI
	//Select ROI
	while(true){
		if(!strcmp(CAPTURE_TYPE, "rpi"))
		{
			RpiCamera.grab();
			RpiCamera.retrieve (rpi_cam_data, raspicam::RASPICAM_FORMAT_RGB);

			if(rpi_cam_data != NULL)
				frame = cv::Mat(RPI_CAMERA_HEIGHT_RESOLUTION, RPI_CAMERA_WIDTH_RESOLUTION, CV_8UC3, rpi_cam_data, 3 * RPI_CAMERA_WIDTH_RESOLUTION);
		}
		else if(!strcmp(CAPTURE_TYPE, "file") || !strcmp(CAPTURE_TYPE, "cam"))
		{
			cap->read(frame);
		}

		if (frame.empty()) {
			printf("Corrupted frame. Aborting...\n");
			break;
		}

		resize(frame, r_frame, Size(IMAGE_WIDTH, IMAGE_HEIGHT), 0, 0, INTER_LINEAR);
		rectangle(r_frame, roi,CV_RGB(255, 0, 0),1,8,0);

		if(web_live_view_is_updated == 0)
		{
			r_frame.copyTo(web_live_view_img);
			web_live_view_is_updated = 1;
		}


		if(drag == 0)
		{
			if(roi.width!=0 && roi.height!=0)
			{
				ecotracker.init(r_frame, roi, parameters);
				break;
			}
		}

		imshow("view", r_frame);
		waitKey(1);
	}
#endif

	//Run Tracker
	while(true){

#ifndef ENABLE_GUI
		static time_t parameters_time;
		if(is_file_modified((char *) "/home/pi/Git/tracker/web/output.web", parameters_time))
		{
			read_web_parameters((char *) "/home/pi/Git/tracker/web/output.web");

			if(roi.width!=0 && roi.height!=0)
				ecotracker.init(r_frame, roi, parameters);
		}
#endif

		if(!strcmp(CAPTURE_TYPE, "rpi"))
		{
			RpiCamera.grab();
			RpiCamera.retrieve (rpi_cam_data, raspicam::RASPICAM_FORMAT_RGB);

			if(rpi_cam_data != NULL)
				frame = cv::Mat(RPI_CAMERA_HEIGHT_RESOLUTION, RPI_CAMERA_WIDTH_RESOLUTION, CV_8UC3, rpi_cam_data, 3 * RPI_CAMERA_WIDTH_RESOLUTION);
		}
		else if(!strcmp(CAPTURE_TYPE, "file") || !strcmp(CAPTURE_TYPE, "cam"))
		{
			cap->read(frame);
		}

		if (frame.empty()) {
			printf("Corrupted frame. Aborting...\n");
			break;
		}

		resize(frame, r_frame, Size(IMAGE_WIDTH, IMAGE_HEIGHT), 0, 0, INTER_LINEAR);

		double timereco = (double)getTickCount();
		bool okeco = ecotracker.update(r_frame, roi);
		float fpseco = getTickFrequency() / ((double)getTickCount() - timereco);

		// draw the tracked object
		if(okeco)
		{
			printf("tracking\n");
			rectangle(r_frame, roi, Scalar( 0, 0, 255 ), 2, 1 );

			char stream[64];
			sprintf(stream, "%6.2f, %6.2f, %6.2f, %6.2f\n", roi.x, roi.y, roi.width, roi.height);

			printf("writing: %s", stream);
			write_serial(serial_fd, stream);

			//need to connect tx-rx to read on rpi
//			printf("reading: ");
//			int c;
//			while((c = read_serial(serial_fd)) != -1)
//			{
//				printf("%c", c);
//			}
//			printf("\n");
		}
		else
		{
			printf("tracking failed\n");
		}

		if(web_live_view_is_updated == 0)
		{
			r_frame.copyTo(web_live_view_img);
			web_live_view_is_updated = 1;
		}

#ifdef ENABLE_GUI
		imshow("view", r_frame);
		waitKey(1);
#endif

		printf("FPS: %6.2lf\n\n", fpseco);
	}
}


