<?php
$bin_path = "/home/pi/Git/tracker/build";
$cmd = "cd " . $GLOBALS['bin_path'] . " && sudo ./tracker rpi 320 240 2 400 >> /home/pi/Git/tracker/web/out.log 2>&1 &";

exec("sudo pgrep tracker", $pgrep_output, $pgrep_return);

if ($pgrep_return == 0) {
    exec("sudo killall tracker");
    sleep(2);
    exec($GLOBALS['cmd'], $output, $return);
}
else
{
    sleep(2);
    exec($GLOBALS['cmd'], $output, $return);
}

sleep(10);

$data = "0,0,0,0";
$ret = file_put_contents('/home/pi/Git/tracker/web/output.web', $data, LOCK_EX);
?>

<html>
<head>
<title>RPI Stream</title>

<link rel="stylesheet" type="text/css" href="css/imgareaselect-default.css" />
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery.imgareaselect.pack.js"></script>

</head>
<body>
<div id="video-canvas" style="text-align: center">
         <img id="image" />
</div>

<script type="text/javascript">

function selectArea(img, selection) {
    if (!selection.width || !selection.height)
        return;
    
    var http = new XMLHttpRequest();
    var url = "roi.php";
    var params = "x1=" + selection.x1 + "&y1=" + selection.y1 + "&x2=" + selection.x2 + "&y2=" + selection.y2;
    http.open("POST", url, true);

    console.log(params);
    
    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            alert(http.responseText);
        }
    }
    http.send(params);
}

$(document).ready(function () {
    $('img#image').imgAreaSelect({
        handles: true,
        autoHide: true,
        onSelectEnd: selectArea
    });
    
    
});
</script>
<script type="text/javascript" src="scripts/index.js"></script>
</body>
</html>



