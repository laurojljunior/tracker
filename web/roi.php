<?php
if (isset($_POST['x1']) && isset($_POST['x2']) && isset($_POST['y1']) && isset($_POST['y2'])) {
    $x1 = $_POST['x1'];
    $y1 = $_POST['y1'];
    $x2 = $_POST['x2'];
    $y2 = $_POST['y2'];
    
    $content = $x1 . "," . $y1 . "," . $x2 . "," . $y2;
    
    $ret = file_put_contents('/home/pi/Git/tracker/web/output.web', $content, LOCK_EX);
    
    echo "Initializing Tracker with ROI (". $content .")";
} else {
    die('No post data to process');
}
?>