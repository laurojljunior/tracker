#include "serial.h"


int open_serial(const char* device, int baud)
{
	int fd;

	if(wiringPiSetup() < 0)
	{
		printf("Unable to start wiringPi: %s\n", strerror (errno)) ;
		return -1;
	}

	if((fd = serialOpen(device, baud)) < 0)
	{
		printf("Unable to open serial device: %s\n", strerror (errno)) ;
		return -1;
	}

	return fd;
}

int write_serial(int fd, char* stream)
{
	for(int i = 0; i < strlen(stream); i++)
	{
		serialPutchar(fd, stream[i]);
	}

	return 0;
}

int read_serial(int fd)
{
	if(serialDataAvail(fd))
		 return serialGetchar (fd);

	return -1;
}

int close_serial(int fd)
{
	serialClose(fd);
	return 0;
}
