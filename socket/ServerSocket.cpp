// Implementation of the ServerSocket class

#include "ServerSocket.hpp"


ServerSocket::ServerSocket ( int port )
{
  if ( ! Socket::create() )
    {
      cout << "Could not create server socket." << endl;
    }

  if ( ! Socket::bind ( port ) )
    {
	  cout << "Could not bind to port." << endl;
    }

  if ( ! Socket::listen() )
    {
	  cout << "Could not listen to socket." << endl;
    }

}

ServerSocket::~ServerSocket()
{
}


const ServerSocket& ServerSocket::operator << ( const std::string& s ) const
{
  if ( ! Socket::send ( s ) )
    {
	  cout << "Could not write to socket." << endl;
    }

  return *this;

}


const ServerSocket& ServerSocket::operator >> ( std::string& s ) const
{
  if ( ! Socket::recv ( s ) )
    {
      cout << "Could not read from socket." << endl;
    }

  return *this;
}

void ServerSocket::accept ( ServerSocket& sock )
{
  if ( ! Socket::accept ( sock ) )
    {
      cout << "Could not accept socket." << endl;
    }
}

void ServerSocket::write ( void* c, size_t size, int* ret )
{
	if ( ! Socket::write ( c, size ) )
	{
		cout << "Could not write on socket. 2" << endl;
		*ret  = 1;
		return;
	}

	*ret = 0;
}
